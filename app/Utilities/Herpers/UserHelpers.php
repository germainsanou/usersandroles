<?php 

namespace App\Utilities\Helpers;

use App\User;

/**
 * 
 */
class UserHelpers
{
	
	public static function DisplayRoles(User $user)
	{
		$roles = '';
		foreach ($user->roles as $role) {
			$roles = $roles . $role->name . ',';
		}

		return trim($roles, ',');
	}

}