<?php

use Illuminate\Database\Seeder;
USE App\User;
use App\Role;

class userTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(App\User::class, 15)->create();

        $admin = User::create([
	        'name' => 'admin',
	        'email' => 'admin@gmail.com',
	        'email_verified_at' => now(),
	        'password' => \Hash::make('password')
	    ]);

	    $utilisateur = User::create([
	        'name' => 'utilisateur',
	        'email' => 'utilisateur@gmail.com',
	        'email_verified_at' => now(),
	        'password' => \Hash::make('password')
	    ]);

	    $auteur = User::create([
	        'name' => 'auteur',
	        'email' => 'auteur@gmail.com',
	        'email_verified_at' => now(),
	        'password' => \Hash::make('password')
	    ]);

	    $adminRole= Role::whereName('admin')->first();
	    $utilisateurRole= Role::whereName('utilisateur')->first();
	    $auteurRole= Role::whereName('auteur')->first();

	    $admin->roles()->attach($adminRole);
	    $auteur->roles()->attach($auteurRole);
	    $utilisateur->roles()->attach($utilisateurRole);
    }
}
