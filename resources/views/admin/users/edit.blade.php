@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">Modifer {{ $user->name }}</div>
            <div class="card-body">
              <form action="{{ route('admin.users.update', $user) }}" method="POST">
                @csrf
                @method('PUT')
                @foreach ($roles as $role)
                  <div class="form-group px-3">
                    <input type="checkbox"  class="form-check-input" name="roles[]" id="{{ $role->id }}" value="{{ $role->id }}" 
                      @foreach ($user->roles as $userRole)
                        @if ($role->id == $userRole->id)
                          {{ "checked" }}
                        @endif
                      @endforeach>
                    <label for="{{ $role->id }}" class="form-check-label">{{ $role->name }}</label>
                  </div>
                @endforeach
                <input type="submit" value="Mettre a jour" class="btn btn-sm btn-primary">
              </form>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection
