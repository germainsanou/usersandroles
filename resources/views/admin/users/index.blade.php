<?php use App\Utilities\Helpers\UserHelpers; ?>
@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header text-center text-secondary">Liste des utilisateurs</div>
          <div class="card-body">
            <table class="mx-auto">
              <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Roles</th>
                <th scope="col">Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($users as $user)
                <tr>
                <th scope="row">{{ $loop->index+1 }}</th>
                <td class="p-1">{{ $user->name }}</td>
                <td class="p1">{{ $user->email }}</td>
                <td class="p1">{{ UserHelpers::displayRoles($user) }}</td>
                <td class="p1">
                  <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-info btn-sm mx-2">Edit</a>

                  @can('delete-user')
                    <form action="{{ route('admin.users.destroy', $user) }}" method="POST" class="d-inline">
                      @csrf
                      @method('DELETE')
                      <input type="submit" value="supprimer" class="btn btn-sm btn-danger">
                    </form>
                  @endcan
                </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection
